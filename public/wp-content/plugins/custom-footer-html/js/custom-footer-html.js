jQuery(document).ready(function() {
	jQuery('.fixed-footer-html-container').addClass('position-bottom');
	jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > 200) {
        	jQuery('.fixed-footer-html-container').removeClass('position-bottom').addClass('position-shift-up');
        } else {
            jQuery('.fixed-footer-html-container').removeClass('position-shift-up').addClass('position-bottom');
        }
    });
});